﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Cryptography.Tests
{
    public class ElipticCurveTests
    {
        public void AddsPoints()
        {
            var elipticCurve = new ElipticCurve(16, 0, 19);
            var p = new ElipticPoint(17, 6, elipticCurve);
            var q = new ElipticPoint(10, 1, elipticCurve);
            var r = p + q;

            var s = p + p;
        }
    }

    public class ElipticPointTests
    {
        [Theory]
        [ClassData(typeof(ElipticPointsAreTheSameClassData))]
        public void ElipticPointsAreTheSame(ElipticPoint a, ElipticPoint b, bool expected)
        {
            var actual = a == b;
            Assert.Equal(expected, actual);
        }

        public class ElipticPointsAreTheSameClassData : IEnumerable<object[]>
        {
            public IEnumerator<object[]> GetEnumerator()
            {
                yield return new object[] 
                {
                    ElipticPoint.Identity,
                    ElipticPoint.Identity,
                    true
                };

                yield return new object[]
                {
                    new ElipticPoint(),
                    ElipticPoint.Identity,
                    false
                };

                yield return new object[]
                {
                    ElipticPoint.Identity,
                    new ElipticPoint(),
                    false
                };

                yield return new object[]
                {
                    new ElipticPoint(),
                    new ElipticPoint(),
                    true
                };

                yield return new object[]
                {
                    new ElipticPoint(1,2,3),
                    new ElipticPoint(1,2,3),
                    true
                };

                yield return new object[]
                {
                    new ElipticPoint(1,2,3),
                    new ElipticPoint(1,2,4),
                    false
                };

                yield return new object[]
                {
                    new ElipticPoint(1,2,3),
                    new ElipticPoint(1,3,3),
                    false
                };


                yield return new object[]
                {
                    new ElipticPoint(1,2,3),
                    new ElipticPoint(2,2,3),
                    false
                };
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }
        }
    }
}
