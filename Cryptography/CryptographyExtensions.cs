﻿using System;
using System.Numerics;

namespace Cryptography
{
    public static class CryptographyExtensions
    {
        public static BigInteger ModPow(this BigInteger a, BigInteger k, BigInteger n)
        {
            var y = BigInteger.One;
            var x = a;
            while (k > 0)
            {
                var lastBit = k & 1;
                if (lastBit == 1)
                    y = (y * x) % n;

                k >>= 1;
                x = (x * x) % n;
            }

            return y;
        }

        public static (BigInteger gcd, BigInteger x, BigInteger y) GCDExtended(this BigInteger a, BigInteger b)
        {
            if (a == 0)
                return (b, 0, 1);

            var (gcd, x1, y1) = GCDExtended(b % a, a);

            var x = y1 - (b / a) * x1;
            var y = x1;

            return (gcd, x, y);
        }

        public static BigInteger ModInverse(this BigInteger a, BigInteger n)
        {
            (_, var x, _) = GCDExtended(a, n);
            return x.LeastPositiveResidue(n);
        }

        public static BigInteger LegendreSymbol(this BigInteger a, BigInteger n)
        {
            var exponent = (n - 1) / 2;
            var modulus = a.ModPow(exponent, n);
            return modulus.LeastAbsoluteResidue(n);
        }

        public static bool IsQuadraticResidue(this BigInteger a, BigInteger n)
        {
            var legendre = a.LegendreSymbol(n);
            return legendre == 1;
        }

        public static BigInteger QuadraticResidueWhenModulusIs3Mod4(this BigInteger a, BigInteger n)
        {
            if (n % 4 != 3)
                throw new ArgumentException("n is not cougruence to 3 mod 4", nameof(n));

            var exponent = (n + 1) / 4;
            var b = a.ModPow(exponent, n);
            return b.LeastPositiveResidue(n);
        }

        public static BigInteger LeastPositiveResidue(this BigInteger a, BigInteger n)
        {
            var residue = a % n;
            return residue < 0 ? residue + n : residue;
        }

        private static BigInteger LeastAbsoluteResidue(this BigInteger a, BigInteger n)
        {
            var other = a < 0 ? a + n : a - n;
            return BigInteger.Abs(a) < BigInteger.Abs(other) ? a : other;
        }
    }
}
