﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Cryptography
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            var s = false;
            var p = BigInteger.Parse("68719476731");
            var a = BigInteger.Parse("49710749469");
            var b = BigInteger.Parse("5286130045");
            var curve = new ElipticCurve(a, b, p);
            var point = new ElipticPoint(24069898531, 10203122697, curve);
            var p2 = new ElipticPoint(24069898531, 10203122697, curve);
            var r = point + point;
            Console.WriteLine(r);

            //var a = BigInteger.Parse("230494833619330872742071433259892253887918924949625621075110348455162899582640562756609713");
            //var b = BigInteger.Parse("225785530019760328737805254637561350820485164307786402015943227616074411698429350907607751");
            //var p = BigInteger.Parse("523133468360889049404922330981983268743289535618129665870465316487757998439707462631766351");

            //var e = new ElipticCurve(a, b, p);

            //var px1 = BigInteger.Parse("175757722742565321624944121270945167081993085306911405305303187535900979958321560252348498");
            //var py1 = BigInteger.Parse("226448623369642016063981880559768023550784441813507797884241707518308435413329534175311217");
            //var qx1 = BigInteger.Parse("304223499801180221986968767003559465876563746859119614741554413137259445936662302970066936");
            //var qy1 = BigInteger.Parse("102134142598989436206400710402192938770351951638395982416611953688718767254279072458296380");
            //var p1 = e.GetPoint(px1, py1);
            //var q1 = e.GetPoint(qx1, qy1);

            //var p1Neq = p1.GenerateNegative();
            //var r1 = p1 + q1;
            //var s1 = p1 + p1;
            //var s2 = q1 + q1;

            //var t1 = p1 + p1Neq;
        }
    }
}
