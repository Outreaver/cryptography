﻿using System;
using System.Numerics;

namespace Cryptography
{
    public struct ElipticCurve
    {
        public BigInteger A { get; }
        public BigInteger B { get; }
        public BigInteger PrimeField { get; }
        public BigInteger Discriminant { get; }

        public bool IsDefined => Discriminant % PrimeField != 0;

        public ElipticCurve(BigInteger a, BigInteger b, BigInteger primeField)
        {
            A = a;
            B = b;
            PrimeField = primeField;
            Discriminant = GetDiscriminant(a, b, primeField);
        }

        public ElipticPoint GetPoint(BigInteger x, BigInteger y)
        {
            return new ElipticPoint(x, y, this);
        }

        public static ElipticCurve Generate(BigInteger primeField)
        {
            var a = primeField.GenerateRandom();
            var b = primeField.GenerateRandom();
            var discriminant = GetDiscriminant(a, b, primeField);
            if (discriminant % primeField == 0)
                return Generate(primeField);

            return new ElipticCurve(a, b, primeField);
        }

        public ElipticPoint GeneratePoint()
        {
            var x = PrimeField.GenerateRandom();
            var y2 = F(x, A, B, PrimeField);
            if (y2.LegendreSymbol(PrimeField) == -1)
                return GeneratePoint();

            var y = y2.QuadraticResidueWhenModulusIs3Mod4(PrimeField);
            return new ElipticPoint(x, y, this);

            static BigInteger F(BigInteger x, BigInteger a, BigInteger b, BigInteger p)
            {
                return (BigInteger.Pow(x, 3) + a * x + b) % p;
            }
        }

        public ElipticPoint GeneratePoint(BigInteger bigInteger)
        {
            var x = bigInteger;
            var y2 = F(x, A, B, PrimeField);
            if (y2.LegendreSymbol(PrimeField) == -1)
                return GeneratePoint();

            var y = y2.QuadraticResidueWhenModulusIs3Mod4(PrimeField);
            return new ElipticPoint(x, y, this);

            static BigInteger F(BigInteger x, BigInteger a, BigInteger b, BigInteger p)
            {
                return (BigInteger.Pow(x, 3) + a * x + b) % p;
            }
        }

        private static BigInteger GetDiscriminant(BigInteger a, BigInteger b, BigInteger primeField)
        {
            return (4 * BigInteger.Pow(a, 3) + 27 * BigInteger.Pow(b, 2)) % primeField;
        }

        public static bool operator ==(ElipticCurve a, ElipticCurve b) => a.A == b.A && a.B == b.B && a.PrimeField == b.PrimeField;
        public static bool operator !=(ElipticCurve a, ElipticCurve b) => (a == b) is false;

        public override string ToString()
        {
            var aStr = A.ToString();
            var bStr = B.ToString();

            var a = A > 1000000000000 ? $"{aStr[..6]}..{aStr[^6..]}" : aStr;
            var b = B > 1000000000000 ? $"{bStr[..6]}..{bStr[^6..]}" : bStr;

            return $"Y^2 = X^3 + {a}X^2 + {b}";
        }

        public override bool Equals(object obj)
        {
            if (obj is not ElipticCurve other)
                return false;

            return this == other;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(A, B, PrimeField);
        }
    }
}