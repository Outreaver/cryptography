﻿using System;
using System.Numerics;

namespace Cryptography
{
    public static class BigIntegerExtensions
    {
        private static readonly Random Random = new();

        public static BigInteger GenerateRandom(this BigInteger maxValue)
        {
            var zeroBasedUpperBound = maxValue - 1; // Inclusive

            var bytes = zeroBasedUpperBound.ToByteArray();

            // Search for the most significant non-zero bit
            byte lastByteMask = 0b11111111;
            for (byte mask = 0b10000000; mask > 0; mask >>= 1, lastByteMask >>= 1)
            {
                if ((bytes[^1] & mask) == mask)
                    break; // We found it
            }

            while (true)
            {
                Random.NextBytes(bytes);
                bytes[^1] &= lastByteMask;
                var result = new BigInteger(bytes);
                if (result <= zeroBasedUpperBound)
                    return result;
            }
        }
    }
}
