﻿using System;
using System.Numerics;

namespace Cryptography
{
    public struct ElipticPoint
    {
        public static ElipticPoint Identity { get; } = new ElipticPoint(true);

        private readonly bool _isIdentity;

        public BigInteger X { get; }
        public BigInteger Y { get; }
        public ElipticCurve ElipticCurve { get; }

        private ElipticPoint(bool isIdentity) : this()
        {
            _isIdentity = isIdentity;
        }

        public ElipticPoint(BigInteger x, BigInteger y, ElipticCurve elipticCurve)
            : this()
        {
            X = x.LeastPositiveResidue(elipticCurve.PrimeField);
            Y = y.LeastPositiveResidue(elipticCurve.PrimeField);
            ElipticCurve = elipticCurve;
        }

        public bool IsNegativeTo(ElipticPoint other)
        {
            return Y + other.Y == ElipticCurve.PrimeField;
        }

        public ElipticPoint GenerateNegative()
        {
            return new ElipticPoint(X, -Y, ElipticCurve);
        }

        public static bool operator ==(ElipticPoint a, ElipticPoint b) => (a._isIdentity && b._isIdentity)
            || (!a._isIdentity && !b._isIdentity && a.X == b.X && a.Y == b.Y && a.ElipticCurve == b.ElipticCurve);
        public static bool operator !=(ElipticPoint a, ElipticPoint b) => (a == b) is false;

        public static ElipticPoint operator +(ElipticPoint p, ElipticPoint r)
        {
            return (p, r) switch
            {
                _ when p == Identity => r,
                _ when r == Identity => p,
                _ when p == r => AddSamePoint(p),
                _ when p.IsNegativeTo(r) => Identity,
                _ => AddTwoDiffrentPoints(p, r)
            };
        }

        private static ElipticPoint AddTwoDiffrentPoints(ElipticPoint p, ElipticPoint r)
        {
            if (p.ElipticCurve != r.ElipticCurve)
                throw new ArgumentException();

            var xDiffrenceInvers = (r.X - p.X).ModInverse(p.ElipticCurve.PrimeField);
            var lambda = (r.Y - p.Y) * xDiffrenceInvers % p.ElipticCurve.PrimeField;
            var x = lambda * lambda - p.X - r.X;
            var y = lambda * (p.X - x) - p.Y;

            return new ElipticPoint(x, y, p.ElipticCurve);
        }

        private static ElipticPoint AddSamePoint(ElipticPoint p)
        {
            var doubleYInverse = (2 * p.Y).ModInverse(p.ElipticCurve.PrimeField);
            var lambda = (3 * p.X * p.X + p.ElipticCurve.A) * doubleYInverse % p.ElipticCurve.PrimeField;
            var x = (lambda * lambda) - (2 * p.X);
            var y = (lambda * (p.X - x)) - p.Y;

            return new ElipticPoint(x, y, p.ElipticCurve);
        }

        public override string ToString()
        {
            if (_isIdentity)
                return "infty"; //return Char.ConvertFromUtf32(0x0001D4AA);

            return $"({X}, {Y}) (mod {ElipticCurve.PrimeField})";
        }

        public override bool Equals(object obj)
        {
            if (obj is not ElipticPoint other)
                return false;

            return this == other;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(X, Y, ElipticCurve);
        }
    }
}